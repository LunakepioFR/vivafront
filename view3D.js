import React, { Component } from 'react';
import ExpoGraphics from 'expo-graphics';
import * as THREE from 'three';

export default class MyScene extends Component {
  onContextCreate = async ({ gl, scale: pixelRatio, width, height }) => {
    // Create a new Three.js scene
    const scene = new THREE.Scene();

    // Create a new Three.js camera
    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
    camera.position.set(0, 0, 5);

    // Create a new Three.js renderer
    const renderer = new ExpoGraphics.Renderer({ gl, pixelRatio, width, height });
    renderer.setSize(width, height);
    renderer.setClearColor(0xffffff);

    // Create a Three.js cube and add it to the scene
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    // Start the animation loop
    const animate = () => {
      requestAnimationFrame(animate);

      // Rotate the cube
      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;

      // Render the scene
      renderer.render(scene, camera);
    };
    animate();
  };

  render() {
    return <ExpoGraphics.View onContextCreate={this.onContextCreate} />;
  }
}
